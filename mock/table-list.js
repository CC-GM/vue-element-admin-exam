const Mock = require('mockjs')

const List = []
const count = 100

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    id: '@increment',
    date: '@date',
    name: '@cname',
    address: '@county(true)'
  }))
}

module.exports = [
  {
    url: '/vue-element-admin/custom/table-list',
    type: 'get',
    response: config => {
      const { page = 1, limit = 20 } = config.query

      const pageList = List.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: List.length,
          items: pageList
        }
      }
    }
  },

  {
    url: '/vue-element-admin/custom/form/add',
    type: 'post',
    response: _ => {
      return {
        code: 20000,
        data: 'success'
      }
    }
  }
]

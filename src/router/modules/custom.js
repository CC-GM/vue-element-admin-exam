
import Layout from '@/layout'

const customRouter = {
  path: '/custom',
  component: Layout,
  name: 'Custom',
  alwaysShow: true,
  meta: {
    title: 'Custom',
    icon: 'form'
  },
  children: [
    {
      path: 'table',
      component: () => import('@/views/custom/table/index'),
      name: 'CustomTable',
      meta: {
        title: 'Custom Table',
        roles: ['editor']
      }
    },
    {
      path: 'form',
      component: () => import('@/views/custom/form/index'),
      name: 'CustomForm',
      meta: {
        title: 'Custom Form',
        roles: ['admin']
      }
    }
  ]
}

export default customRouter
